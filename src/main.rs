use chrono::Local;
use clap::Clap;

//use std::fs;
use fs_err as fs;

use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

#[derive(Clap, Debug)]
#[clap(version = "0.1.0", author = "Tim Heaney <oylenshpeegul@pm.me>")]
struct Opts {
    /// Debug mode
    #[clap(long)]
    debug: bool,

    /// Mark the new post as a draft?
    #[clap(long)]
    draft: bool,

    /// Create the new post in this directory
    #[clap(short, long, parse(from_os_str), default_value = "./content/posts")]
    dir: PathBuf,

    /// Tags are space-separated words
    #[clap(long, default_value = " ")]
    tags: String,

    /// Title of new post (default: today's date)
    #[clap(short, long)]
    title: Option<String>,

    /// Verbose mode (-v, -vv, -vvv, etc.)
    #[clap(short, long, parse(from_occurrences))]
    verbose: u8,
}

fn main() {
    let opts: Opts = Opts::parse();
    if opts.debug {
        eprintln!("{:#?}", opts);
    }

    let mut path = opts.dir;
    fs::create_dir_all(&path).expect("Cannot create path!");

    // Default title is the current date.
    let title = match opts.title {
        Some(s) => s,
        None => Local::now().format("%Y%m%d").to_string(),
    };

    path.push(&title);
    fs::create_dir(&path).expect("Cannot create directory!");

    path.push("index.org");
    let mut file = File::create(&path).expect("Cannot create file!");
    let mut contents = format!("#+title: {}\n", &title);
    contents.push_str(&format!("#+date: {}\n", Local::now().naive_local().date()));
    contents.push_str(&format!("#+draft: {}\n", opts.draft));
    contents.push_str(&format!("#+tags[]: {}\n", opts.tags));

    file.write_all(contents.as_bytes())
        .expect("Cannot write contents to file!");

    file.sync_all().expect("Cannot sync file!");

    if opts.verbose > 0 {
        println!("Contents written to {:?}", &path);
    }
}
